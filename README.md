
# #weatherChecker

Polls weather data (location name, temperature, wind speed and humidity) via openweathermap.org API and stores it to database every 15min. 
You can add as many locations to poll as you wish. Delete function will delete both the location to poll and all the associated weather data. 

To do:
- dockerize everything (app and DB)
- front-end to React.js

Once to-do list is complete:
Clone & run docker project and open at:
http://localhost:8090/

!You need to insert your own API key to the application.properties. 
