let storeWeather = document.getElementById("storeWeather")
storeWeather.addEventListener('click', function () {
    fetch('/cityToPoll', {
        method: 'PUT',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            cityName: newCity.value,
        })
    })
        .then(function (response) {
            database_storage_message.innerHTML = "Storing weather data for " + newCity.value;
        })
        .then(function (response) {
            this.newCity.value = "";
        })
    setTimeout(function (response) {
        database_storage_message.innerHTML = "";
    }, 3000)
})

let deleteCity = document.getElementById("deleteWeather")
deleteCity.addEventListener('click', function () {
    fetch('/deleteCity', {
        method: 'PUT',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            cityName: deleteLocation.value,
        })
    })
        .then(function (response) {
            database_delete_message.innerHTML = "Deleted all the data for " + deleteLocation.value;
        })
        .then(function (response) {
            deleteLocation.value = "";
        })
    setTimeout(function (response) {
        database_delete_message.innerHTML = "";
    }, 3000)
})

let checkWeather = document.getElementById("checkWeather")
checkWeather.addEventListener('click', function () {
    fetch('returnWeather', {
        method: 'POST',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            cityName: checkCity.value,
        })
    })
        .then(function (response) {
            return response.json();
        })
        .then(function (jsonData) {
            var first = jsonData[0];
            if (first == null) {
                display_current_weather.innerHTML = "No weather information in the database";
                setTimeout(function (response) {
                    display_current_weather.innerHTML = "";
                }, 3000)
            } else {
                var html = '<table>';
                html += '<tr>';
                html += '<td align="left"><b>City</b></td>';
                html += '<td align="center"><b>Temperature</b></td>';
                html += '<td align="center"><b>Wind speed</b></td>';
                html += '<td align="center"><b>Humidity</b></td>';
                html += '<td align="center"><b>Date & time</b></td>';
                html += '</tr>';
                for (var i = 0; i < jsonData.length; ++i) {
                    var item = jsonData[i];
                    html += '<tr>';
                    html += '<td align="left">' + item.cityName + '</td>';
                    html += '<td align="center">' + item.temperature + '</td>';
                    html += '<td align="center">' + item.windSpeed + '</td>';
                    html += '<td align="center">' + item.humidity + '</td>';
                    html += '<td align="center">' + item.date + '</td>';
                    html += '</tr>';
                }
                html += '</table>';
                display_current_weather.innerHTML = html;
            }
        })
        .then(function (response) {
            checkCity.value = "";
        });
});