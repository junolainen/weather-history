package weatherHistory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WeatherHistoryService {
    @Autowired
    private WeatherHistoryRepository weatherHistoryRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${openweathermap.Apikey}")
    private String apiKey;

    @Value("${openweathermap.address}")
    private String address;


    // Writes an entry to the "city_polling" table, using the input to make an API request.
    public void cityToPoll(String cityName) {
        UriComponentsBuilder url = UriComponentsBuilder.fromHttpUrl(address)
                .queryParam("q", cityName)
                .queryParam("appId", apiKey)
                .queryParam("units", "metric");
        Map response = restTemplate.getForObject(url.toUriString(), HashMap.class);
        String city = (String) response.get("name");
        weatherHistoryRepository.cityToPoll(city);
    }

    // Returns polled weather information from "weather" table.
    public List returnWeather(String cityName) {
        return weatherHistoryRepository.returnWeather(cityName);
    }

    // Deletes information both from "city_polling" and "weather" table.
    public void deleteCity(String cityName) {
        weatherHistoryRepository.deleteCity(cityName);
        weatherHistoryRepository.deleteCityPoll(cityName);
    }

    // Scheduled task.
    // Fetches location information from "city_polling" table and makes an API request.
    // Writes the information to "weather" table.
    public void updateData() {
        List<InsertWeatherRequest> insertWeatherRequests = weatherHistoryRepository.cityPollingRequest();

        for (int i = 0; i < insertWeatherRequests.size(); i++) {
            UriComponentsBuilder url = UriComponentsBuilder.fromHttpUrl(address)
                    .queryParam("q", insertWeatherRequests.get(i).getCityName())
                    .queryParam("appId", apiKey)
                    .queryParam("units", "metric");

            Map response = restTemplate.getForObject(url.toUriString(), HashMap.class);
            String city = (String) response.get("name");
            Long epoch = Long.valueOf((Integer) response.get("dt"));
            Date convertDate = new Date(epoch * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MMMMM yyyy, HH:mm");
            String date = sdf.format(convertDate);

            Map main = (Map) response.get("main");
            Number temperature = (Number) main.get("temp");
            Number humidity = (Number) main.get("humidity");
            Map wind = (Map) response.get("wind");
            Number windSpeed = (Number) wind.get("speed");
            weatherHistoryRepository.insertWeatherData(city, date, temperature, humidity, windSpeed);
        }
    }

    public void insertWeather(String cityName) {
        UriComponentsBuilder url = UriComponentsBuilder.fromHttpUrl(address)
                .queryParam("q", cityName)
                .queryParam("appId", apiKey)
                .queryParam("units", "metric");

        Map response = restTemplate.getForObject(url.toUriString(), HashMap.class);
        String city = (String) response.get("name");
        Long epoch = Long.valueOf((Integer) response.get("dt"));
        Date convertDate = new Date(epoch * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MMMMM yyyy, HH:mm");
        String date = sdf.format(convertDate);

        Map main = (Map) response.get("main");
        Number temperature = (Number) main.get("temp");
        Number humidity = (Number) main.get("humidity");
        Map wind = (Map) response.get("wind");
        Number windSpeed = (Number) wind.get("speed");
        weatherHistoryRepository.insertWeatherData(city, date, temperature, humidity, windSpeed);
    }

    public void setAddress(String address) {
        this.address = address;
    }
}