package weatherHistory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WeatherHistoryRepository {
    @Autowired
    private NamedParameterJdbcTemplate weatherHistoryDatabase;

    // Inserts a location name to "city_polling" table
    public void cityToPoll(String city) {
        String sql = "INSERT INTO city_polling(city_poll) VALUES (:city)";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("city", city);
        weatherHistoryDatabase.update(sql, paramMap);
    }

    // Returns all the polled information form "weather" table (name, temperature, wind speed, humidity).
    public List returnWeather(String cityName) {
        String sql = "SELECT * FROM weather WHERE city_name = :cityName";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("cityName", cityName);
        List<ReturnWeatherRequest> resultList = weatherHistoryDatabase.query(sql, paramMap, new RequestWeatherRowMapper());
        return resultList;
    }

    // Deletes entries from "weather" table.
    public void deleteCity(String cityName) {
        String sql = "DELETE FROM weather WHERE city_name = :cityName";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("cityName", cityName);
        weatherHistoryDatabase.update(sql, paramMap);
    }

    // Deletes entries from "city_polling" table.
    public void deleteCityPoll(String cityName) {
        String sql = "DELETE FROM city_polling WHERE city_poll = :cityName";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("cityName", cityName);
        weatherHistoryDatabase.update(sql, paramMap);
    }
    // Returns location from "city_polling" table to be used with the API request.
    public List<InsertWeatherRequest> cityPollingRequest() {
        String sql = "SELECT city_poll FROM city_polling";
        return weatherHistoryDatabase.query(sql, new HashMap<>(), new CityPollingRowMapper());
    }

    // Inserts weather data from API request to the table "weather".
    public void insertWeatherData(String city, String date, Number temperature,
                                  Number humidity, Number windSpeed) {
        String sql = "INSERT INTO weather(city_name, date, temperature, humidity, wind_speed)" +
                "VALUES (:city, :date, :temperature, :humidity, :windSpeed)";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("city", city);
        paramMap.put("date", date);
        paramMap.put("temperature", temperature);
        paramMap.put("humidity", humidity);
        paramMap.put("windSpeed", windSpeed);
        weatherHistoryDatabase.update(sql, paramMap);
    }
}