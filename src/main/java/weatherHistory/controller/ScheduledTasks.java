package weatherHistory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {
    @Autowired
    private WeatherHistoryService weatherHistoryService;

    @Scheduled(fixedRate = 900000)
    public void pollToDatabase() {
        weatherHistoryService.updateData();
    }
}