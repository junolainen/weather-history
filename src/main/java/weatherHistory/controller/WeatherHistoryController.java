package weatherHistory.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WeatherHistoryController {
    @Autowired
    private WeatherHistoryService weatherHistoryService;

    // Writes city name to "city_polling" table in order for @Scheduled to get the name and start polling
    @PutMapping("cityToPoll")
    public void cityToPoll(@RequestBody InsertWeatherRequest request) {
        weatherHistoryService.cityToPoll(request.getCityName().replace(" ", "+"));
    }

    // Returns all the polled data from "weather" table
    @PostMapping("returnWeather")
    public List returnWeather(@RequestBody ReturnWeatherRequest request) {
        return weatherHistoryService.returnWeather(request.getCityName());
    }

    //deletes data from "weather" table.
    @PutMapping("deleteCity")
    public void deleteCity(@RequestBody DeleteCityRequest request) {
        weatherHistoryService.deleteCity(request.getCityName());
    }

    // Writes info from API to the "weather" table. No function currently.
    // Was used to add manual entries from API to "weather" table.
    @PutMapping("checkCity")
    public void insertWeather(@RequestBody InsertWeatherRequest request) {
        weatherHistoryService.insertWeather(request.getCityName().replace(" ", "+"));
    }
}