package weatherHistory.controller;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityPollingRowMapper implements RowMapper<InsertWeatherRequest> {
    @Override
    public InsertWeatherRequest mapRow(ResultSet resultSet, int i) throws SQLException {
        InsertWeatherRequest insertWeatherRequests = new InsertWeatherRequest();
        insertWeatherRequests.setCityName(resultSet.getString("city_poll"));
        return insertWeatherRequests;
    }
}