package weatherHistory.controller;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RequestWeatherRowMapper implements RowMapper<ReturnWeatherRequest> {
    @Override
    public ReturnWeatherRequest mapRow(ResultSet resultSet, int i) throws SQLException {
        ReturnWeatherRequest weatherData = new ReturnWeatherRequest();
        weatherData.setCityName(resultSet.getString("city_name"));
        weatherData.setTemperature(resultSet.getFloat("temperature"));
        weatherData.setWindSpeed(resultSet.getFloat("wind_speed"));
        weatherData.setHumidity(resultSet.getInt("humidity"));
        weatherData.setDate(resultSet.getString("date"));
        return weatherData;
    }
}